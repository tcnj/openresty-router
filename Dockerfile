FROM openresty/openresty:xenial
MAINTAINER Danny Wensley <dannywensley7@gmail.com>

ENV REDIS_PORT=6379
ENV REDIS_HOST=redis

ADD nginx.conf /usr/local/openresty/nginx/conf/nginx.conf